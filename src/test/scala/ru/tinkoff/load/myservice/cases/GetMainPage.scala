package ru.tinkoff.load.myservice.cases

import io.gatling.http.Predef._
import io.gatling.core.Predef._

object GetMainPage {

  val getMainPage = http("GET /")
    .get("/get")
    .header("accept", "application/json")
    .check(status is 200)

}
